package org.test;


import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class App {

    private static final String INPUT_PATH = "/data.tsv";
    private static final String OUTPUT_PATH = "corrected_data.tsv";

    public static void main(String[] args) throws IOException {
        URL resource = App.class.getResource(INPUT_PATH);
        if (resource == null) {
            throw new FileNotFoundException(INPUT_PATH);
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(resource.getFile()),
                StandardCharsets.UTF_16LE))) {

            try (PrintWriter resultFile = new PrintWriter(new BufferedWriter(new FileWriter(OUTPUT_PATH)))) {

                //skip first line as it contains headers
                br.readLine();

                String line;
                Person currentValue = null;
                while ((line = br.readLine()) != null) {
                    String[] values = Arrays.stream(line.split("\t"))
                            .filter(f -> !f.isBlank())
                            .toArray(String[]::new);

                    if (currentValue != null && currentValue.isValid()) {
                        resultFile.println(currentValue);
                        currentValue = null;
                    }

                    if (values.length == 5) {
                        Integer id = parseId(values[0]);
                        if (id == null) {
                            currentValue = null;
                            continue;
                        }

                        Integer accNumber = parseAccNumber(values[3]);
                        if (accNumber == null) {
                            currentValue = null;
                            continue;
                        }

                        currentValue = new Person(id, values[1], values[2], accNumber, values[4]);
                        resultFile.println(currentValue);
                        currentValue = null;
                    } else {

                        if (currentValue == null)
                            currentValue = new Person();

                        int i = 0;

                        if (currentValue.id == 0) {
                            Integer id = parseId(values[i++]);
                            if (id == null) {
                                currentValue = null;
                                continue;
                            }

                            currentValue.id = id;
                        }


                        if (currentValue.firstName == null && values.length > i)
                            currentValue.firstName = values[i++];

                        if (currentValue.lastName == null && values.length > i)
                            currentValue.lastName = values[i++];

                        if (currentValue.accNumber == 0 && values.length > i) {
                            Integer accNumber = parseAccNumber(values[i++]);
                            if (accNumber == null) {
                                currentValue = null;
                                continue;
                            }

                            currentValue.accNumber = accNumber;
                        }

                        if (currentValue.email == null && values.length > i) {
                            String value = values[i];
                            if (value.contains("@"))
                                currentValue.email = value;
                            else {
                                log("Not an email: %s in %s. Skipping", value, line);
                                currentValue = null;
                            }

                        }

                    }

                }
            }
        }

    }


    private static Integer parse(String s) {
        return Integer.parseInt(s.replace("/", "").trim());
    }

    private static Integer parseId(String s) {
        try {
            return parse(s.replace("/", "").trim());
        } catch (NumberFormatException e) {
            log("Expected id, got %s. Skipping", s);
        }
        return null;
    }

    private static Integer parseAccNumber(String s) {
        try {
            return parse(s.replace("/", "").trim());
        } catch (NumberFormatException e) {
            log("Expected accNumber, got %s. Skipping", s);
        }
        return null;
    }

    // Utility methods for logging

    private static void log(String line, String s) {
        System.err.println(String.format(line, s));
    }

    private static void log(String line, String s, String s2) {
        System.err.println(String.format(line, s, s2));
    }

}
