package org.test;

class Person {
    long id;
    String firstName, lastName;
    long accNumber;
    String email;

    Person() {
    }

    Person(long id, String firstName, String lastName, long accNumber, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accNumber = accNumber;
        this.email = email;
    }

    @Override
    public String toString() {
        return "" + id + '\t' +
                firstName + '\t' +
                lastName + '\t' +
                accNumber + '\t' +
                email;
    }

    boolean isValid() {
        return id != 0 && firstName != null && lastName != null && accNumber != 0 && email != null;
    }
}
