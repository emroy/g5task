package org.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcNewOutputFormat;
import org.apache.hadoop.hive.ql.io.orc.OrcSerde;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class App extends Configured implements Tool {

    public static class ORCMapper extends Mapper<LongWritable, Text, NullWritable, Writable> {

        private final OrcSerde serde = new OrcSerde();

        //Define the struct which will represent each row in the ORC file
        private final String typeString = "struct<id:int,first_name:string,last_name:string,acc_number:int,email:string>";

        private final TypeInfo typeInfo = TypeInfoUtils.getTypeInfoFromTypeString(typeString);
        private final ObjectInspector oip = TypeInfoUtils.getStandardJavaObjectInspectorFromTypeInfo(typeInfo);

        private final Person person = new Person();

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            person.parse(value.toString());
            if (person.parse(value.toString())) {
                List<Object> struct = new ArrayList<>(4);
                struct.add(0, person.getId());
                struct.add(1, person.getFirstName());
                struct.add(2, person.getLastName());
                struct.add(3, person.getAccNumber());
                struct.add(4, person.getEmail());
                Writable row = serde.serialize(struct, oip);
                context.write(NullWritable.get(), row);
            }
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length < 2) {
            System.err.printf("Usage: %s [generic options] <input> <output> <numTasks: optional>\n",
                    getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }

        Configuration conf = getConf();

        //Set ORC configuration parameters
        conf.set("orc.create.index", "true");

        Job job = Job.getInstance(conf, "Text to Parquet");
        job.setJarByClass(getClass());

        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(Writable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));

        Path output = new Path(args[1]);

        job.setMapperClass(ORCMapper.class);

        int numTasks = 1;
        if (args.length == 3) {
            try {
                numTasks = Integer.parseInt(args[2]);
            } catch (NumberFormatException ignored) {

            }
        }

        job.setNumReduceTasks(numTasks);

        job.setOutputFormatClass(OrcNewOutputFormat.class);
        OrcNewOutputFormat.setCompressOutput(job, true);
        OrcNewOutputFormat.setOutputPath(job, output);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new App(), new String[]{"/code/_JAVA/_GARBAGE/g5task/corrected_data.tsv",
                "/code/_JAVA/_GARBAGE/g5task/out.data", "2"});

        System.exit(exitCode);
    }
}