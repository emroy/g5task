package org.test;


class Person {
    private int id;
    private String firstName, lastName;
    private int accNumber;
    private String email;

    int getId() {
        return id;
    }


    String getFirstName() {
        return firstName;
    }

    String getLastName() {
        return lastName;
    }

    int getAccNumber() {
        return accNumber;
    }

    String getEmail() {
        return email;
    }


    boolean parse(String line) {
        try {
            String[] values = line.split("\t");
            id = Integer.parseInt(values[0]);
            firstName = values[1];
            lastName = values[2];
            accNumber = Integer.parseInt(values[3]);
            email = values[4];
            return true;

        } catch (Exception ignored) {
            return false;

        }
    }
}
