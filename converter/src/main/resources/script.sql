CREATE EXTERNAL TABLE person
(
id INT,
fist_name VARCHAR(50),
last_name VARCHAR(50),
acc_number INT,
email VARCHAR(50)
)
STORED AS ORC
LOCATION '/out.data';